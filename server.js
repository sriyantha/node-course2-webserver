const express = require('express');
const fileSystem = require('fs');
const port = process.env.PORT || 3000;

const hbs = require('hbs');
hbs.registerPartials(__dirname + '/views/partials');

var app = express();
app.set('view engine', 'hbs');
app.use(express.static(__dirname + '/public'));

app.use((req, res, next) =>{
  var now = new Date().toString();
  var log = 'Log:' + now + ',' + req.method +',' + req.url;
  console.log(log);
  fileSystem.appendFile('server.log',log + '\n', (err) =>{
    if(err){
      console.log('Unable to append to server.log');
    }
  });
  next();
});

/* app.use((request, response, next) =>{
  response.render('maintenance.hbs');
  next();
}); */

hbs.registerHelper('getCurrentYear', () =>{
  return new Date().getFullYear();
});

hbs.registerHelper('screamIt', (text) =>{
  return text.toUpperCase();
});

app.get('/', (request, response) =>{
  //response.send('<h1>Hello Express !!!</h1>');
  response.send({
    name:'Stanley',
    likes : ['Reading', 'Watching movies']
  });
});

app.get('/about',(request, response) =>{
  response.render('about.hbs', {
    pageTitle: 'About Page',
  });
});

app.get('/home',(request, response) =>{
  response.render('home.hbs', {
    pageTitle: 'Home Page',
    user: 'Stanley',
  });
});

app.get('/project', (request, response) =>{
  response.render('project.hbs', {
    pageTitle: 'Project portfolio Page',
    user: 'Stanley',
  });
});

app.get('/bad', (request, response) =>{
  response.send({
    status : 'failed',
    errorMessage : 'Request cannot be fulfilled due to an unspecified error'
  });
});



app.listen(port, () =>{
  console.log('Server is up on port:' + port);
});
